﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Alien_Run
{
    class Player
    {
        // ATTRIBUTES
        // Variables
        Texture2D spr;
        Vector2 pos = new Vector2(50, 50);
        Vector2 vel;
        Rectangle bbox;
        Level level;

        // Constants
        const float MOVE_ACCEL = 10f;
        const float MAX_SPD = 200f;
        const float GRAVITY_ACCEL = 400f;
        const float TERMINAL_VEL = 850f;

        // METHODS
        public Player(Level newLevel)
        {
            level = newLevel;
        }

        public void LoadContent(ContentManager content)
        {
            spr = content.Load<Texture2D>("player/player-stand");
            bbox = new Rectangle((int)pos.X , (int)pos.Y, spr.Width, spr.Height);
        }

        public Rectangle GetBounds()
        {
            return bbox;
        }
        public void SetBounds(Vector2 position)
        {
            bbox.X = (int)(position.X);
            bbox.Y = (int)(position.Y);
        }

        public void TileCollision()
        {
            List<Tile> collidingTiles = level.GetTilesInBounds(bbox);

            foreach (Tile t in collidingTiles)
            {
                var tileBounds = t.GetBounds();
                var depth = CollisionDepth(bbox, tileBounds);

                // If intersection has occured
                if (depth != Vector2.Zero)
                {
                    var absDepthX = Math.Abs(depth.X);
                    var absDepthY = Math.Abs(depth.Y);

                    if (absDepthY < absDepthX)
                        pos.Y += depth.Y;
                    else
                        pos.X += depth.X;

                    SetBounds(pos);
                }
            }
        }

        private Vector2 CollisionDepth(Rectangle player, Rectangle tile)
        {
            // Calculate half of the player and tile sizes
            Vector2 playerHalf = new Vector2(player.Width / 2, player.Height / 2);
            Vector2 tileHalf = new Vector2(tile.Width / 2, tile.Height / 2);

            // Calculate distance between centres
            Vector2 dist = player.Center.ToVector2() - tile.Center.ToVector2();

            // Calculate minimum non-overlapping distance
            Vector2 minDist = playerHalf + tileHalf;
            
            // If there is no intersection return 0
            if (Math.Abs(dist.X) >= minDist.X || Math.Abs(dist.Y) >= minDist.Y)
                return Vector2.Zero;
            // Calculate collision depth
            else
            {
                Vector2 depth = Vector2.Zero;

                // Calculate x depth
                if (dist.X > 0)
                    depth.X = minDist.X - dist.X;
                else
                    depth.X = -minDist.X - dist.X;

                // Calculate y depth
                if (dist.Y > 0)
                    depth.Y = minDist.Y - dist.Y;
                else
                    depth.Y = -minDist.Y - dist.Y;

                return depth;
            }
        }

        public void Update(GameTime gameTime)
        {
            // Update position
            var deltaT = (float)gameTime.ElapsedGameTime.TotalSeconds;

            vel.X = MathHelper.Min(vel.X + MOVE_ACCEL * deltaT, MAX_SPD);
            vel.Y = MathHelper.Clamp(vel.Y + GRAVITY_ACCEL * deltaT, -TERMINAL_VEL, TERMINAL_VEL);

            pos += vel * deltaT;

            SetBounds(pos);

            // Check for collisions
            TileCollision();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(spr, pos, null, Color.White);
        }
    }
}
