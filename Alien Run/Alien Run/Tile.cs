﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Alien_Run
{
    class Tile
    {
        // ATTRIBUTES
        Texture2D spr;
        Vector2 pos;
        Rectangle bbox;

        // METHODS
        public Tile(Texture2D sprite, Vector2 position)
        {
            spr = sprite;
            pos = position;
            bbox = new Rectangle((int)pos.X, (int)pos.Y, spr.Width, spr.Height);
        }

        public Rectangle GetBounds()
        {
            return bbox;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (spr != null)
                spriteBatch.Draw(spr, pos, Color.White);
        }
    }
}
