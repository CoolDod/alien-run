﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Alien_Run
{
    class Level
    {
        // ATTRIBUTES
        Texture2D tile_box,
            tile_bridge;

        const int TILE_SIZE = 70;
        const int X_TILES = 13;
        const int Y_TILES = 7;
        const int FLOOR_Y = 350;
        const int SCREENS = 5;

        Tile[,] tile_map = new Tile[X_TILES * SCREENS, Y_TILES];
        List<Tile> tiles = new List<Tile>();

        // METHODS
        public void CreateTile(int cell_x, int cell_y, Texture2D sprite)
        {
            var tile = new Tile(sprite, new Vector2(cell_x * TILE_SIZE, cell_y * TILE_SIZE));

            tile_map[cell_x, cell_y] = tile;
            tiles.Add(tile);
        }

        public void LoadContent(ContentManager content)
        {
            tile_box = content.Load<Texture2D>("tiles/box");
            tile_bridge = content.Load<Texture2D>("tiles/bridge");

            // TILES
            // First screen
            for (var i = 0; i < X_TILES; i++)
            {
                CreateTile(i, 5, tile_box);
                CreateTile(i, 6, tile_box);
            }
        }

        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            List<Tile> inBounds = new List<Tile>();

            int leftTile = (int)Math.Floor((double)bounds.Left / TILE_SIZE);
            int rightTile = (int)Math.Ceiling((double)bounds.Right / TILE_SIZE) - 1;
            int topTile = (int)Math.Floor((double)bounds.Top / TILE_SIZE);
            int bottomTile = (int)Math.Ceiling((double)bounds.Bottom / TILE_SIZE) - 1;

            for (var i = leftTile; i <= rightTile; i++)
            {
                for (var j = topTile; j <= bottomTile; j++)
                {
                    if(tile_map[i, j] != null)
                        inBounds.Add(tile_map[i, j]);
                }
            }

            return inBounds;
        }

        public void Update(GameTime gameTime)
        {
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Tile t in tiles)
                t.Draw(spriteBatch);
        }
    }
}
